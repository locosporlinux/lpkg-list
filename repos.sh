#!/bin/bash

# l10n
sh_lang=$(echo $LANG | cut -d. -f1)
case $sh_lang in
    es*) sudo_warn="Sin permiso de superusuario no se va a poder ¯\_(ツ)_/¯"
         need_fzf_warn="Instalando fzf..."
         question="¿Estás usando Loc-OS 22 o Loc-OS 23?"
         impossible="Imposible"
         choice="Elige la región de los repositorios que quieres usar"
         eewarn="Anonyzard: No hagas cosas raras por favor"
         ;;
    *)   sudo_warn="Whitout permission from superuser, it will not be possible ¯\_(ツ)_/¯"
         need_fzf_warn="Installing fzf..."
         question="Are you using Loc-OS 22 or Loc-OS 23?"
         impossible="Impossible"
         choice="Choice the region of repositories you want to use"
         eewarn="Anonyzard: Don't do rare things please"
         ;;
esac

# Verify if run as superuser
if [ "$(id -u)" -ne 0 ]; then
    echo "$sudo_warn"
    exit 1
fi

# Verify if fzf is installed
if ! which fzf > /dev/null 2>&1; then
    echo $need_fzf_warn
    fail=false
    apt install fzf -y || fail=true
    if $fail; then
        exit 2
    fi
   
fi

# Chose Loc-OS version
version=$(echo "$question
Loc-OS 22
Loc-OS 23" | fzf --header-lines=1 --layout=reverse)

if [ "$version" = "Loc-OS 22" ]; then
    codename="elloco"
elif [ "$version" = "Loc-OS 23" ]; then
    codename="contutti"
else
    echo $impossible
    exit 3
fi

# Write result to sources.list and check keyring file
write() {
    source=/etc/apt/sources.list.d/loc-os.list
    if [ "$1" = "elloco" ]; then
        source=/etc/apt/sources.list.d/loc-os22.list
    fi
    echo "$2" > $source

    keyring_file="/etc/apt/trusted.gpg.d/loc-os-archive-keyring.gpg"
    repo=$(echo "$2" | tail -1 | cut -d\  -f2 )
    if ! [ -f $keyring_file ]; then
        wget "$repo/loc-os-archive-keyring.gpg" -o $keyring_file
    fi
}

# Repos
usa="# USA
deb http://us.loc-os.com $codename main"

brazil="# BRAZIL
deb http://br.loc-os.com $codename main"

france="# FRANCE
deb http://fr.loc-os.com $codename main"

all="$usa
$brazil
$france"

# Choice repos
variants=$(echo "$choice
USA
Brazil
France
ALL" | fzf --header-lines=1 --layout=reverse)

case "$variants" in
    "USA") write "$codename" "$usa";;
    "Brazil") write "$codename" "$brazil";;
    "France") write "$codename" "$france";;
    "ALL") write "$codename" "$all";;
    *) echo "$eewarn";;
esac

exit 0
